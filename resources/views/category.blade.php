<!DOCTYPE html>
    <html lang='{{ $locale ?? 'en' }}'>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Internationalisation (i18n)</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
        <!-- Bootstrap from cdn -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body class="antialiased">
        <div class="container relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
                    <h1>i18n</h1>
                </div>

                <div class="container">
                    <div class="form-group">
                        <label >{{__("language.select-language")}}</label>
                        <select id="languageSelector">
                            <option value="select">{{__("language.select")}}</>
                            <option value="en">{{__("language.english")}}</>
                            <option value="fr">{{__("language.french")}}</>
                        </select>
                        <input type="button" value="{{ __("language.apply-choice") }}" onclick="applychoice()" class="btn btn-primary btn-sm rounded" />
                    </div>
                </div>

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid grid-cols-1 md:grid-cols-12">

                        <div class="p-12 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">                            
                            <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm p-3">
                                @if ( !isset($products) )
                                    @if ( isset($message) )
                                        {{ $message }}
                                    @else
                                        <p class="text-center">No products found in this category. Running the <a href='{{ route('populate') }}'>factory</a> will make some</p>
                                    @endif
                                @else
                                    <h3>{{ __("language.".$products[0]->category) }}</h3>
                                    <table class="table table-striped table-dark table-bordered">
                                        <tr>
                                            <th>ID</th>
                                            <th>{{ __("language.category") }}</th>
                                            <th>{{ __("language.name") }}</th>
                                            <th>{{ __("language.description") }}</th>
                                            <th>{{ __("language.price") }}</th>
                                            <th>{{ __("language.Edit") }}?</th>
                                            <th>{{ __("language.Delete") }}?</th>
                                        </tr>
                                        @foreach ($products as $product)
                                            <tr>
                                                <td>{{ $product->id }}</td>
                                                <td><a href="{{ route('updateproducts', $product->category) }}">{{ __("language.".$product->category) }}</a></td>
                                                <td>{{ __("language.".$product->name) }}</td>
                                                <td>{{ __("language.".$product->description) }}</td>
                                                <td>{{ __("language.currency")." ".$product->price }}</td>
                                                <td><a class="btn btn-primary btn-sm" href="update/{{$product->category}}">{{ __("language.Edit") }}</a></td>
                                                <td>
                                                    <form method="get" action="{{route('deleteproducts', $product->category)}}">
                                                        {{ csrf_field() }}
                                                        <input class="btn btn-primary btn-sm" type="submit" value="{{ __("language.Delete") }}" />
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endif
                                <p class="text-center"><a href="{{route('products')}}" class="btn btn-primary btn-sm">{{ __("language.Back") }}</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex justify-center mt-4 sm:items-center sm:justify-between">
                    <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
                        Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
                    </div>
                </div>
            </div>
        </div>
    </body>

    <script>
        function applychoice() { 
            let locale = document.querySelector('#languageSelector').value;
            expiry = new Date();
            expiry.setTime( expiry.getTime()+(3600*60*1000) );
            document.cookie='i18n-language-choice='+locale+'; expires='+ expiry.toGMTString() + ';path=/';
            location.reload(true);
        }
    </script>
</html>
