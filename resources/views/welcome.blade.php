<!DOCTYPE html>
<html lang='{{ $locale ?? 'en' }}'>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Internationalisation (i18n)</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <!-- Bootstrap from cdn -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body class="antialiased">
        <div class="container relative items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
                    <h1>i18n</h1>
                </div>

                <div class="container">
                    <div class="form-group">
                        <label >{{__("language.select-language")}}</label>
                        <select id="languageSelector">
                            <option value="select">{{__("language.select")}}</>
                            <option value="en">{{__("language.english")}}</>
                            <option value="fr">{{__("language.french")}}</>
                        </select>
                        <input type="button" value="{{ __("language.apply-choice") }}" onclick="applychoice()" class="btn btn-primary btn-sm rounded" />
                    </div>
                </div>

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow rounded mt-5">
                    <div class="grid grid-cols-1 md:grid-cols-6 text-center home-page-option-buttons">
                        <div class="p-12 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                            <div class="p-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm p-3">
                                    <a class="btn btn-primary btn-sm rounded mr-2 ml-2" href={{ route('populate') }}>{{ __("language.make-products") }}?</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a class="btn btn-primary btn-sm rounded" href={{ route('products') }}>{{ __("language.view-products") }}?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex justify-center mt-4 sm:items-center sm:justify-between">
                    <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
                        Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
                    </div>
                </div>
            </div>
        </div>
    </body>

    <script>
        function applychoice() { 
            let locale = document.querySelector('#languageSelector').value;
            expiry = new Date();
            expiry.setTime( expiry.getTime()+(3600*60*1000) );
            document.cookie='i18n-language-choice='+locale+'; expires='+ expiry.toGMTString() + ';path=/';
            location.reload(true);
        }
    </script>
</html>
