<?php

return [

    /*
    |--------------------------------------------------------------------------
    | French strings used across the site
    |--------------------------------------------------------------------------
    */

    'select-language' => 'Choisissez votre langue',
    'select' => 'Choisissez',
    'english' => 'Anglaise',
    'french' => 'Français',
    'apply-choice' => 'Appliquer le choix',
    'make-products' => 'Fabriquer des produits',
    'view-products' => 'Voir les produits',
    'category' => 'Catégorie',
    'name' => 'Nom',
    'description' => 'La description',
    'price' => 'Prix',
    'action' => 'Action',
    'Cookers' => 'Cuisinières',
    'Pots' => 'Casseroles',
    'Cutlery' => 'Coutellerie',
    'All products' => 'Tous les produits',
    'Product One' => 'Produit un',
    'Product Two' => 'Product deux',
    'Product Three' => 'Product trois',
    'Useful in a great number of circumstances' => 'Utile dans un grand nombre de circonstances',
    'Great value for money' => 'Excellent rapport qualité/prix',
    'Some excellent artistry on display here' => 'Un excellent artyon s\'affiche ici',
    'currency' => '£',
    'Edit' => 'Éditer',
    'Delete' => 'Effacer',
    'Edit the products in the' => 'Modifiez les produits dans le',
    'category below' => 'catégorie ci-dessous',
    'In English' => 'en anglais',
    'In French' => 'en français',
    'New category name' => 'Nouveau nom de catégorie',
    'Both languages are required to create a language item' => 'Les deux langues sont requises pour créer un élément de langue',
    'Back' => 'Arrière',
    'Spoons' => 'Cuillères',
    'Forks' => 'Fourches',
    'Chairs' => 'Chaises',
    'Spice rack' => 'Étagère à épices',
];