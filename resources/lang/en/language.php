<?php

return [

    /*
    |--------------------------------------------------------------------------
    | English strings used across the site
    |--------------------------------------------------------------------------
    */

    'select-language' => 'Select your language',
    'select' => 'Select',
    'english' => 'English',
    'french' => 'French',
    'apply-choice' => 'Apply choice',
    'make-products' => 'Make products',
    'view-products' => 'View products',
    'category' => 'Category',
    'name' => 'Name',
    'description' => 'Description',
    'price' => 'Price',
    'action' => 'Action',
    'Cookers' => 'Cookers',
    'Pots' => 'Pots',
    'Cutlery' => 'Cutlery',
    'All products' => 'All products',
    'Product One' => 'Product One',
    'Product Two' => 'Product Two',
    'Product Three' => 'Product Three',
    'Useful in a great number of circumstances' => 'Useful in a great number of circumstances',
    'Great value for money' => 'Great value for money',
    'Some excellent artistry on display here' => 'Some excellent artistry on display here',
    'currency' => '£',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'Edit the products in the' => 'Edit the products in the',
    'category below' => 'category below',
    'In English' => 'in English',
    'In French' => 'in French',
    'New category name' => 'New category name',
    'Both languages are required to create a language item' => 'Both languages are required to create a language item',
    'Back' => 'Back',
    'Spoons' => 'Spoons',
    'Forks' => 'Forks',
    'Chairs' => 'Chairs',
    'Spice rack' => 'Spice rack',
];