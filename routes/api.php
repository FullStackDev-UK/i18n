<?php

use App\Models\Product;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Removed middleware('auth:sanctum'), as authentication not required
Route::post( 'product/update', function( Request $request ) {
    $newCategoryName = $request->newCategoryName;
    $newCategoryNameInFrench = $request->newCategoryNameInFrench;
    $category = $request->category;

    if ( $newCategoryName !== $category ) {
        Product::where( 'category', '=', $category )->update(['category' => $newCategoryName]);
        
        // Update the language fields with the French version from the request
        $currentEnglishLanguageArray = file_get_contents('../resources/lang/en/language.php');
        $currentEnglishLanguageArrayTrimmed = rtrim($currentEnglishLanguageArray, '];');
        $newEnglishLanguageStrings = "    '".$newCategoryName."'" . " => " . "'".$newCategoryName."'" . ",\n];";
        file_put_contents('../resources/lang/en/language.php', $currentEnglishLanguageArrayTrimmed.$newEnglishLanguageStrings);

        $currentFrenchLanguageArray = file_get_contents('../resources/lang/fr/language.php');
        $currentFrenchLanguageArrayTrimmed = rtrim($currentFrenchLanguageArray, '];');
        $newFrenchLanguageStrings = "    '".$newCategoryName."'" . " => " . "'".$newCategoryNameInFrench."'" . ",\n];";
        file_put_contents('../resources/lang/fr/language.php', $currentFrenchLanguageArrayTrimmed.$newFrenchLanguageStrings);
    }
    return response()->json( 'All products in category have been updated', 200 );
});

Route::get('/populate', function () {
    Artisan::call('db:seed');
    return response()->json( Product::all() );
});

Route::get( 'product/{category}', function( $category ) {
    return response()->json( Product::where('category', $category )->get(), 200 );
});

Route::get( 'product/delete/{category}', function( $category ) {
    Product::where( 'category', $category )->delete();
    return response()->json( 'All products have been deleted from category', 200 );
});

Route::middleware('getlanguage' )->get('/products', function ( Request $request ) {
    $language = $request->cookie( 'i18n-language-choice' ) ?? 'en';

    $products = collect( Product::all() )->each( function( $product ) use ( $language ) {
        // TODO Translation via API routes requires further investigation
        return $product->category = trans($product->category, [], $language);
    });
    return response()->json( $products, 200 );
});