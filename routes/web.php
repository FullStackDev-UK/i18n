<?php

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

// Acceptance criteria one
// Design and implement a RESTful API webservice to select, update and delete products for a given category
// The following is a web site to perform said operations, along with adding new language strings when a category is to be updated. 

// Acceptance criteria two
// Add internationalisation to the service - 
// Given a locale (en-gb, fr-ch) return the product adapted for that locale. 
// You may alter the database as needed, including creating new tables if required.

// First I created a Model, Migration, Controller, Factory and Seeder to create a Product dataset to work with.
// I then added two links to the welcome page, one to create some (10) sample Products, and another to view the created products.
// The link endpoints are '/populate' and '/products', respectively

// The following 'web' routes provide a graphical interface which allows new language items to be added to the language files
// This can also be done via API. Please see the 'api' routes file for the API routes
Route::middleware('getlanguage' )->group( function () {
    Route::get('/', function () {
        return view('welcome' );
    })->name('home');

    // Route to create sample products with a factory call
    Route::get( 'populate', [ ProductController::class, 'populate' ] )->name('populate');

    Route::get( 'products', [ ProductController::class, 'index' ] )->name('products');
    Route::get( 'product/{category}', [ ProductController::class, 'productsByCategory' ] )->name('bycategory');
    Route::get( 'product/update/{category}/', [ ProductController::class, 'updateProducts' ] )->name('updateproducts'); // Returns the view to update the category

    Route::post( 'product/update/{category}', function( $category, Request $request ) {
        $newCategoryName = $request->newCategoryName;
        $newCategoryNameInFrench = $request->newCategoryNameFrench;

        if ( $newCategoryName !== $category ) {
            Product::where( 'category', '=', $category )->update(['category' => $newCategoryName]);
            
            // Update the language fields with the French version from the request
            $currentEnglishLanguageArray = file_get_contents('../resources/lang/en/language.php');
            $currentEnglishLanguageArrayTrimmed = rtrim($currentEnglishLanguageArray, '];');
            $newEnglishLanguageStrings = "    '".$newCategoryName."'" . " => " . "'".$newCategoryName."'" . ",\n];";
            file_put_contents('../resources/lang/en/language.php', $currentEnglishLanguageArrayTrimmed.$newEnglishLanguageStrings);

            $currentFrenchLanguageArray = file_get_contents('../resources/lang/fr/language.php');
            $currentFrenchLanguageArrayTrimmed = rtrim($currentFrenchLanguageArray, '];');
            $newFrenchLanguageStrings = "    '".$newCategoryName."'" . " => " . "'".$newCategoryNameInFrench."'" . ",\n];";
            file_put_contents('../resources/lang/fr/language.php', $currentFrenchLanguageArrayTrimmed.$newFrenchLanguageStrings);
        }
        return view ('category')->with('message', 'All products in category have been updated');
    })->name('productupdate');

    Route::get( 'product/delete/{category}', function( $category ) {
        Product::where( 'category', $category )->delete();
        return view ('category')->with('message', 'All products have been deleted from category');
    })->name('deleteproducts');
});

Route::fallback( function() {
    return 'Sorry, that product does not exist, so this would usually be a nicely-designed 404 page<br /><a href="products">Back</a>.';
});