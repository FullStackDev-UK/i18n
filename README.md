# i18n

Internationalisation (i18n) api/site with GUI to write new, corresponding, EN and FR language terms to Laravel's php language files in the resources/lang folder.
Also, custom middleware checks for if a language cookie has been set (using the dropdown and JavaScript on each Blade which I'll DRY up with extends later), and applies the choice of language to the current request.

# To Run

1. Clone the repo into your local webserver
2. Change directory into the directory that's just been created
3. Run composer install:

`composer install`

4. Create a local database, called anything you want, and put the name of it in the DB_DATABASE= field in the .env file along with your other database credentials.

5. Allow write permission on the files required to run the site:

`chmod 777 resources/lang/en/language.php` \
`chmod 777 resources/lang/fr/language.php`

6. Allow write permissions on the storage and bootstrap folders:

`sudo chmod 777 ./storage/ ./bootstrap/ -R`

7. Run the database migrations

`php artisan migrate`

8. Run npm install and npm run watch

9. Go to http://localhost/i18n/public/ (or http://127.0.0.1/i18n/public/ if that's how you're set up) and you should see the site.

10. If it's your first visit, click the 'Make products' link to create some products. Then click the 'Back' button and 'View products' to view the products you've just made. Then change language and add items at will.
