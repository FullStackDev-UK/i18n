<?php

namespace Database\Factories;

use App\Models\Product;
use Faker\Core\Number;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement([
                'Product One', 
                'Product Two', 
                'Product Three'
            ]),
            'description' => $this->faker->randomElement([
                'Some excellent artistry on display here', 
                'Great value for money', 
                'Useful in a great number of circumstances'
            ]),
            'category' => $this->faker->randomElement([
                'Cookers', 
                'Pots', 
                'Cutlery'
            ]),
            'price' => ( new Number )->randomNumber(2),
        ];
    }
}
